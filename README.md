# CVWS.NET: Computer Vision Wordsearch Solver .NET #
A Computer Vision system word search solver.
# Project moved to [Github](https://github.com/JoshKeegan/CVWS.NET) #
Visit the project website at [http://cvws.joshkeegan.co.uk/](http://cvws.joshkeegan.co.uk/)